---
title: "Les vaccins sont-t-il dangereux?"
date: 2020-05-22T16:06:14+02:00
description: "Informations"
tags: [ "vaccins", "santé", "controverse" ]
draft: false
---
Depuis des années on nous dit que les vaccins sont sure, efficace et ne comportent aucun risques. Cet avis est-il unanime? 

Il y a de plus en plus de doutes émis envers les vaccins et l'argumentaire est solide.

Je partage en vrac quelques liens, je prendrais le temps de rédiger plus tard.

## Mauvaise définition de la controverse

La plupart des gens ne remettent pas en cause les vaccins et leur utilité mais les nouvelles méthodes de fabrications, les adjuvants et le manque de suivi. Seulement on met dans le même panier une personne qui a des doutes souhaitant plus de transparence et une personne qui est anti-chimie et qui ne veut rien entendre.

Les adjuvants comme l'hydroxyde d'aluminium sont des neurotoxiques reconnu, on dit qu'en intramusculaire et à faible dose ils sont vite éliminés par le corps. Certains parents veulent des preuves. Sachant que chez les animaux cela a été reconnu comme cause d'une cancer appelés [sarcomes](https://professeur-joyeux.com/2019/05/16/moi-laluminium-nai-rien-a-faire-vaccin/) 

## Manque de transparence

Les parents véritablement touchés par les effets secondaires des vaccins ne peuvent former des groupes en ligne pour informer d'autres parents([procès Zuckerberg](https://youtu.be/yvyFSEs7Zv0),[Lutte contre les anti-vax](https://youtu.be/61ZH0eARG4M)). 

Tout médecin émettant des doutes sur les neurotoxiques comme le professeur Joyeux est considéré comme Hérétique. Le simple fait d'avoir quelques désaccords avec la politique de vaccination fait de lui un [complotiste](https://youtu.be/tImhNseYdEI).
Rappelons qu'il avait fait une [Pétition](https://webcache.googleusercontent.com/search?q=cache:k1ttP8x0V4kJ:https://www.ipsn.eu/1-million-de-signatures-pour-le-retour-du-vaccin-dt-polio-sans-aluminium/+&cd=3&hl=fr&ct=clnk&gl=fr) pour que les vaccins obligatoires ne soient pas mélangés avec les vaccins non obligatoires et surtout qu'ils soient fournie en version sans aluminium afin d'avoir au moins d'aussi bon soins que [les chats](https://www.vaccinssansaluminium.org/le-saviez-vous/).

## Mensonges des institutions(aux US)

[Les vaccins n'ont pas fait leurs preuves](https://youtu.be/9JG5b8Qt_CY) comme l'indique le résultat de ce procès [l'ICAN vs CDC - mai 2020](/PDF/ICAN-VS-CDC-LAWSUIT-PR.pdf) ou bien encore de [celui ci datant de 2016](/PDF/vaccin_are_unavoidably_unsafe_1609.pdf). Pire encore ils démontrent qu'**aucun des tests** que s'engageaient à faire ces institutions n'ont été fait et ce **depuis 32ans**. Il ne reste donc d'un coté la [bonne foi](https://www.instagram.com/p/B-7cRNPHrA2/?utm_source=ig_web_copy_link) des société pharmaceutiques et de l'autre des études qui tendent à montrer que les adjuvants sont néfastes ainsi que des [nouvelles inquiétantes](https://economictimes.indiatimes.com/industry/healthcare/biotech/healthcare/controversial-vaccine-studies-why-is-bill-melinda-gates-foundation-under-fire-from-critics-in-india/articleshow/41280050.cms), voir même [terribles](https://childrenshealthdefense.org/news/government-corruption/gates-globalist-vaccine-agenda-a-win-win-for-pharma-and-mandatory-vaccination/) [controversées en France](https://factuel.afp.com/non-le-vaccin-contre-la-polio-promu-par-bill-gates-na-pas-paralyse-pres-de-500-000-enfants-en-inde).

## Pour en savoir plus

* [Manque d'informations sur les vaccins multi-valences et conflits d'interêts](https://youtu.be/oAoSS8g_0Es?t=1055)
* [Échange informationnel avec le Pr Joyeux et Pr Montagnier Nov2017](https://youtu.be/SOw2a7wXuLs)
* [Fabrication et contrôle des vaccins au niveau industriel - Sanofi](https://www.canal-u.tv/video/canal_u_medecine/cif_vaccinologie_2011_fabrication_et_controle_des_vaccins.7080)
* [Impossibilité de discuter de la formule des vaccins au Senat(dialogue de sourds)](https://youtu.be/ebTxnqUGurE)
* [Vaccin sans adjuvants pour le bien être des chats](https://www.vaccinssansaluminium.org/le-saviez-vous/)
* [Article bien sourcé sur les opérations de vaccination moins bien réussies en inde et afrique](http://afrohistorama.over-blog.com/2014/10/vaccins-l-inde-poursuit-la-fondation-bill-et-melinda-gates.html)
* [Les blogueurs font le travail de journaliste](https://youtu.be/luGjpaKulaE)
* [Judy Mikovits](https://www.bitchute.com/video/EyThPJo8IneY/)
